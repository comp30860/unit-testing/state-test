import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CalculatorUnitTest {
    private Calculator calculator;

    @Before public void setupCalculator() {
        calculator = new CalculatorImpl();
    }

    @Test public void testAddition() {
        assertEquals("2+5=7", 7,calculator.add(2,5));
    }
}

